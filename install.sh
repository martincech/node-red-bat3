#!/bin/bash

if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi


LOCAL_ADDRESS="127.0.0.1"
REMOTE_ADDRESS=""

SERVICE_PATH="/lib/systemd/system"
INFLUXDB_SERVICE=""
SERVICES=($INFLUXDB_SERVICE)

DownloadAndExtract() 
{
   	sudo apt-get install -y build-essential;
	sudo apt-get install nodejs;
	sudo apt-get install npm;	
	npm install
}


#ReplaceVariables()
#{
# ;
#ls {}

UninstallAndDelete()
{
	npm uninstall
}

#CopyServices()
#{
#;
#}

RegisterServices()
{  
   for prop in ${SERVICES[@]}; 
   do
     RegisterService $prop
   done
}

RegisterService()
{
	echo "registering " 
	echo $SERVICE_PATH/$1
   systemctl enable $SERVICE_PATH/$1
}

UnRegisterServices()
{
   for prop in ${SERVICES[@]}; 
   do
     UnRegisterService $prop
   done
}

UnRegisterService()
{
   systemctl disable $1
}

StartServices()
{
   for prop in ${SERVICES[@]}; 
   do
     StartService $prop
   done
}

StartService()
{
   systemctl start $1
}

StopServices()
{
   for prop in ${SERVICES[@]}; 
   do
     StopService $prop
   done
}

StopService()
{
   systemctl stop $1
}

StatusServices()
{
   for prop in ${SERVICES[@]}; 
   do
     StatusService $prop
   done
}

StatusService()
{
   systemctl status $1
}


ShowHelp() 
{
   echo " -h			Show this help"
   echo " -i			Download and install all the files and services and start them"
   echo " start			Start the services"
   echo " stop			Stop the services"
   echo " register		Register the services"
   echo " deregister	Deregister the services"
   echo " status		Status the services"
   echo " -u			Uninstall all the services and delete files"
}

case "$1" in
   -u)   echo "!! ->    Uninstalling ...."  
         StopServices
         sleep 1 
         UnRegisterServices
         UninstallAndDelete
         ;;
   -i)   echo  "!! ->  Installing ...."
         DownloadAndExtract
         RegisterServices
         ;;
   register)
         echo  "!! ->  Registering services ...."
         RegisterServices
         ;;
   deregister)
         echo  "!! ->  Unregistering services ...."
         UnRegisterServices
         ;;
   start)
         echo  "!! ->  Starting services ...."
         StartServices
         ;;
   stop) echo  "!! ->  Stopping services ...."
         StopServices         
         ;;
   status)
         StatusServices         
         ;;
   log)
         for prop in ${SERVICES[@]}; 
         do
            journalctl -u $prop > /media/sf_shared/$prop.log
         done
         
         ;;
   *)    ShowHelp
         ;;
esac




